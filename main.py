from selenium.webdriver.common.keys import Keys
import datetime, logging, time, random
from urllib.parse import urlencode
import urllib.parse as urlparse
from selenium import webdriver
from bs4 import BeautifulSoup
# from data_fun import insert_sql

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(module)s - %(thread)d - %(funcName)s - %(lineno)d - %(message)s')
file_handler = logging.FileHandler('main.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)


def linkedin_login(driver, username, password):
    try:
        driver.get("https://www.linkedin.com/uas/login?trk=guest_homepage-basic_nav-header-signin")
        driver.find_element_by_id("username").click()
        driver.find_element_by_id("username").send_keys(username)
        time.sleep(random.randint(1, 6))
        driver.find_element_by_id("password").send_keys(password)
        time.sleep(random.randint(1, 6))
        driver.find_element_by_id("password").send_keys(Keys.ENTER)
        logger.info('Login Done')
        return driver
    except Exception as e:
        print('Exception in Linkedin Login : ', e)
        pass


def search_sn(driver, geography, industry, seneiority, company_type, head_count):
    pop_url = "https://www.linkedin.com/sales/search/people"
    try:
        print("Searching Sales Navigator")
        driver.get(pop_url)
        logger.info('Sales Navigator')
        # Adding Keyword
        # time.sleep(5)
        # print("Input Search Keyword")
        # driver.find_element_by_xpath('/html/body/div[3]/div/div/div[2]/div/div[1]/div/section[1]/ul/li[1]/div/div/div/div[1]/div/input').click()
        # driver.find_element_by_xpath('/html/body/div[3]/div/div/div[2]/div/div[1]/div/section[1]/ul/li[1]/div/div/div/div[1]/div/input').send_keys(keyword)
        # driver.find_element_by_xpath('/html/body/div[3]/div/div/div[2]/div/div[1]/div/section[1]/ul/li[1]/div/div/div/div[1]/div/input').send_keys(Keys.ENTER)
        # time.sleep(random.randint(1, 6))
    except Exception as e:
        print("Exception in inserting keyword : ", e)
        pass

    # try:
    #     # Finding Results
    #     print("clicking search button")
    #     driver.find_element_by_class_name("button-primary-medium").click()
    # except Exception as e:
    #     print("Exception in clicking save button : ", e)
    #     pass
    time.sleep(5)
    # print("current_url : ",driver.current_url)

    encoded_geography = encode_geography(geography)
    encoded_industries = encode_industries(industry)
    encoded_seniority = encode_seniority(seneiority)
    encoded_company_type = encode_company_type(company_type)
    encoded_head_count = encode_head_count(head_count)
    # print(encoded_geography, encoded_company_name)
    filtered_url = add_filter(driver.current_url, geography=encoded_geography, industries=encoded_industries, seniority=encoded_seniority,
                              company_type=encoded_company_type, head_count=encoded_head_count, )
    # filtered_url = add_filter(driver.current_url,seniority=encoded_seniority, )

    driver.get(filtered_url)
    time.sleep(4)
    return driver


def encode_geography(geographies):
    "Function to encode geographies to linkedin-stack codes"

    geography_dict = {"Afghanistan": "101240012", "Albania": "102845717", "Algeria": "106395874", "Andorra": "106296266", "Angola": "105371935",
                      "Antigua and Barbuda": "100270819", "Argentina": "100446943", "Armenia": "103030111", "Australia": "101452733",
                      "Austria": "103883259", "Azerbaijan": "103226548", "Bahamas": "106662619", "Bahrain": "100425729","Bangladesh": "106215326",
                      "Barbados": "102118611", "Belarus": "101705918", "Belgium": "100565514", "Belize": "105912732", "Benin": "101519029",
                      "Bhutan": "103613266", "Bolivia": "104379274", "Bosnia and Herzegovina": "102869081", "Botswana": "105698121", "Brazil": "106057199",
                      "Brunei": "103809722", "Bulgaria": "105333783", "Burkina Faso": "100587095", "Burundi": "100800406", "Côte d'Ivoire": "103295271",                      "Cabo Verde": "101679268",
                      "Cambodia": "102500897", "Cameroon": "105745966", "Canada": "101174742", "Central African Republic": "100134827", "Chad": "104655384",
                      "Chile": "104621616", "China": "102890883", "Colombia": "100876405", "Comoros": "103069791", "Congo (Congo-Brazzaville)": "106796687",
                      "Costa Rica": "101739942", "Croatia": "104688944", "Cuba": "106429766", "Cyprus": "106774002", "Democratic Republic of the Congo": "101271829",
                      "Denmark": "104514075", "Djibouti": "100371290", "Dominica": "100720695", "Dominican Republic": "105057336", "Ecuador": "106373116",
                      "Egypt": "106155005", "El Salvador": "106522560", "Equatorial Guinea": "105141335", "Eritrea": "104219996", "Ethiopia": "100212432",
                      "Fiji": "105733447", "Finland": "100456013", "France": "105015875", "Gabon": "104471338", "Gambia": "106100033", "Georgia": "106315325",
                      "Germany": "101282230", "Ghana": "105769538", "Greece": "104677530", "Grenada": "104579260", "Guatemala": "100877388",
                      "Guinea": "100099594", "Guinea-Bissau": "100115557", "Guyana": "105836293", "Haiti": "100993490", "Vatican City": "107163060",
                      "Honduras": "101937718", "Hungary": "100288700", "Iceland": "105238872", "India": "102713980", "Indonesia": "102478259", "Iran": "101934083",
                      "Iraq": "106725625", "Ireland": "104738515", "Israel": "101620260", "Italy": "103350119", "Jamaica": "105126983", "Japan": "101355337",
                      "Jordan": "103710677", "Kazakhstan": "106049128", "Kenya": "100710459", "Kiribati": "104742735", "Kuwait": "103239229",
                      "Kyrgyzstan": "103490790", "Laos": "100664862", "Latvia": "104341318", "Lebanon": "101834488", "Lesotho": "103712797",
                      "Liberia": "106579411", "Libya": "104036859", "Liechtenstein": "100878084", "Lithuania": "101464403", "Luxembourg": "104042105",
                      "Madagascar": "105587166", "Malawi": "105992277", "Malaysia": "106808692", "Maldives": "102161637", "Mali": "100770782", "Malta": "100961908",
                      "Marshall Islands": "106516799", "Mauritania": "106950838", "Mauritius": "106931611", "Mexico": "103323778", "Micronesia": "102762290",
                      "Moldova": "106178099", "Monaco": "101352147", "Mongolia": "102396337", "Montenegro": "100733275", "Morocco": "102787409", "Mozambique": "100474128",
                      "Myanmar": "104136533", "Namibia": "106682822", "Nauru": "100348836", "Nepal": "104630404", "Netherlands": "102890719", "New Zealand": "105490917",
                      "Nicaragua": "105517145", "Niger": "103550069", "Nigeria": "105365761", "North Korea": "102356536", "Macedonia": "105604166",
                      "Norway": "103819153", "Oman": "103619019", "Pakistan": "101022442", "Palau": "106779877", "Palestine": "101381088", "Panama": "100808673",
                      "Papua New Guinea": "100152180", "Paraguay": "104065273", "Peru": "102927786", "Philippines": "103121230", "Poland": "105072130",
                      "Portugal": "100364837", "Qatar": "104170880", "Romania": "106670623", "Russia": "101728296", "Rwanda": "103547315", "St Kitts and Nevis": "102098694",
                      "St Lucia": "104022923", "Saint Kitts and Nevis": "102098694", "Saint Lucia": "104022923", "Saint Vincent and the Grenadines": "104703990",
                      "St Vincent and the Grenadines": "104703990", "Samoa": "104031721", "San Marino": "105730022", "Sao Tome and Principe": "106867470",
                      "Saudi Arabia": "100459316", "Senegal": "103587512", "Serbia": "101855366", "Seychelles": "107007050", "Sierra Leone": "106524187",
                      "Singapore": "102454443", "Slovakia": "103119917", "Slovenia": "106137034", "Solomon Islands": "104980134", "Somalia": "104725424",
                      "South Africa": "104035573", "South Korea": "105149562", "South Sudan": "103622308", "Spain": "105646813", "Sri Lanka": "100446352",
                      "Sudan": "106740205", "Suriname": "105530931", "Sweden": "105117694", "Switzerland": "106693272", "Syria": "106714035", "Tajikistan": "105925962",
                      "Tanzania": "104604145", "Thailand": "105146118", "Timor-Leste": "101101678", "Togo": "103603395", "Tonga": "105646927", "Trinidad and Tobago": "106947126",
                      "Tunisia": "102134353", "Turkey": "102105699", "Turkmenistan": "105449295", "Tuvalu": "103609605", "Uganda": "106943612", "Ukraine": "102264497",
                      "United Arab Emirates": "104305776", "United Kingdom": "101165590", "United States of America": "103644278", "Uruguay": "100867946",
                      "Uzbekistan": "107734735", "Vanuatu": "102185308", "Venezuela": "101490751", "Vietnam": "104195383", "Yemen": "105962095",
                      "Zambia": "102120260", "Zimbabwe": "101367137"}
    output = []
    try:
        for geography in geographies.split(","):
            print("geography : ", geography)
            output.append(geography_dict[geography.strip()])
        return ",".join(output)
    except Exception as e:
        logger.exception("Exception in encoding geography : {}".format(str(e)))
        return "102713980"


def encode_industries(industries):
    "Function to encode industries to linkedin-stack codes"

    comp = {"Computer Networking": "5", "Computer Software": "4", "Information Services": "84", "Information Technology and Services": "96"}
    output = []
    try:
        for industry in industries.split(","):
            print("industry : ", industry)
            # output.append(industries_dict[industry.strip()])
            output.append(comp[industry.strip()])
        return ",".join(output)
    except Exception as e:
        logger.exception("Exception in encoding industries : {} ".format(str(e)))
        return "96"


def encode_seniority(seniorities):
    "Function to encode seniorities to linkedin-stack codes"

    seniority_dict = {"Owner": "10", "Partner": "9", "CXO": "8", "VP": "7", "Director": "6", "Manager": "5", "Senior": "4", "Entry": "3", "Training": "2",
                      "Unpaid": "1", }
    output = []
    try:
        for seniority in seniorities.split(","):
            print("seniority : ", seniority)
            output.append(seniority_dict[seniority.strip()])
        return ",".join(output)
    except Exception as e:
        logger.exception("Exception in encoding seniority : {}".format(str(e)))
        return "8"


def encode_company_type(company_type):
    "Function to encode company_include to linkedin-stack codes"

    company_type_dict = {"Partnership": "S", "Privately Held": "P", "Self Employed": "E", "Self Owned": "O"}
    output = []
    try:
        for company_type in company_type.split(","):
            print("company_type : ", company_type)
            output.append(company_type_dict[company_type.strip()])
        return ",".join(output)
    except Exception as e:
        logger.exception("Exception in encoding company_type : {}".format(str(e)))
        return "0"


def encode_head_count(head_count):
    "Function to encode head_count to linkedin-stack codes"

    head_count_dict = {"1-10": "B", "11-50": "C", "51-200": "D"}
    output = []
    try:
        for head_count in head_count.split(","):
            print("head_count : ", head_count)
            output.append(head_count_dict[head_count.strip()])
        return ",".join(output)
    except Exception as e:
        logger.exception("Exception in encoding head_count : {}".format(str(e)))
        return "0"


def add_filter(url, geography, industries, seniority, company_type, head_count):
    "Function to update geography in url"

    params = {'geoIncluded': geography, 'industryIncluded':industries,  'seniorityIncluded': seniority, 'companyType': company_type, 'companySize': head_count}

    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)
    url_parts[4] = urlencode(query)
    final_url = urlparse.urlunparse(url_parts)
    return final_url


def make_url(url, page_no):
    "Function to make a url with page numbers"
    params = {'page': page_no}
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)
    url_parts[4] = urlencode(query)
    final_url = urlparse.urlunparse(url_parts)
    return final_url


def get_pagecount(driver):
    "Function to find total results and return pages count"
    page_count = 0
    try:
        text_div = driver.find_element_by_class_name("artdeco-spotlight-tab__primary-text").text
        print("total results div : ", text_div)
        total_pages = driver.find_element_by_class_name("search-results__pagination-list").text
        print("total pages: ", total_pages[-1])
        page_count = int(total_pages[-1])
        # page_count = round(int("".join(re.findall("\d+", text_div)))/25)
    except Exception as e:
        print("Exception in finding Total Results Count : ", e)
        pass
    return page_count


def extract_profiles(driver, campaign_id):
    """Function to Extract Profile links from result page"""
    print("Extract Profile Information from scraped page")
    # html_content = driver.page_source
    # soup = BeautifulSoup(html_content, 'html.parser')
    #
    # div = soup.find("ul", {"class": "search-filter__list collapsible-container is-expanded ember-view"})
    # sub_div = div.find_all("li", {"class": "search-filter__collapsible-section collapsible-container ember-view"})
    # for i in sub_div:
    #     inner_div = i.find("div", {"class": "relative search-filter ember-view"})
    #     in_div = inner_div.find("div", {"class": "ember-view"})
    #     if "Geography" in in_div.text:
    #         try:
    #             driver.find_element_by_xpath('//*[@id="{}"]/div/button'.format(in_div.get("id"))).click()
    #             logger.info("Clickling on Geography")
    #         except Exception as e:
    #             logger.info("Exception in clickling geography - {}".format(e))
    #
    #         try:
    #             driver.find_element_by_xpath('//*[@id="ember{}-typeahead-region"]'.format(int(str(in_div.get("id")).replace("ember", ""))-1)).send_keys(geography)
    #             logger.info("Typing in Geography")
    #             time.sleep(2)
    #         except Exception as e:
    #             logger.info("Exception in typing geography - {}".format(e))
    #
    #         try:
    #             driver.find_element_by_xpath('/html/body/div[5]/main/div[1]/form/ul/li[4]/div/div/div[2]/ol/li[1]/button').click()
    #             logger.info("Clicking the first")
    #             time.sleep(2)
    #         except Exception as e:
    #             logger.info("Exception in clicking the first - {}".format(e))
    #     elif "Company" in in_div.text:
    #         try:
    #             driver.find_element_by_xpath('//*[@id="{}"]/div/button'.format(in_div.get("id"))).click()
    #             logger.info("Clickling on Comapny")
    #         except Exception as e:
    #             logger.info("Exception in clickling company - {}".format(e))
    #
    #         try:
    #             driver.find_element_by_xpath('//*[@id="ember{}-typeahead"]'.format(int(str(in_div.get("id")).replace("ember", ""))-1)).send_keys(company_name)
    #             logger.info("Typing in Company")
    #             time.sleep(2)
    #         except Exception as e:
    #             logger.info("Exception in typing company - {}".format(e))
    #
    #         try:
    #             driver.find_element_by_xpath('/html/body/div[5]/main/div[1]/form/ul/li[6]/div/div/div[2]/ol/li[1]/button').click()
    #             logger.info("Clicking the first")
    #             time.sleep(2)
    #             break
    #         except Exception as e:
    #             logger.info("Exception in clicking the first - {}".format(e))
    #     else:
    #         pass
    main_url = driver.current_url
    page_count = get_pagecount(driver=driver)
    print("page_count : ", page_count)
    if page_count == 0:
        pc = 2
    else:
        pc = page_count

    for i in range(0, pc):
        new_url = make_url(url=main_url, page_no=i+1)

        print('finding profile urls for this page : ', new_url)
        driver.get(new_url)
        time.sleep(10)
        print("Get URL, Now Scrolling to Bottom")

        for i in range(0, 2):
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            print("scrolling to bottom")
            time.sleep(random.randint(5, 10))

        main_div = driver.find_elements_by_tag_name("article")
        print("Total Profile URLs : ", len(main_div))
        page_urls = []

        for sub_div in main_div:
            print("***** Finding Profile Url *****")
            try:
                url = sub_div.find_element_by_tag_name("a").get_attribute("href")
                print("URL  : ", url)

                if "https://www.linkedin.com/sales/search/company" not in url and "https://www.linkedin.com/sales/company" not in url:
                    page_urls.append(url)

            except Exception as e:
                print("Exception in getting profile urls : ", e)
                pass

        for url in page_urls:
            try:
                print("Finding Data from Profile URL : ", url)

                # Scraping Data from URL
                profile_detail = scrap_single_profile(url=url, driver=driver, campaign_id=campaign_id)
                # print("Profile Detail  : ", profile_detail, "\nGeography: ", geography, "\nSeniority: ", seniority)
                print(profile_detail)
            except Exception as e:
                print("Exception in getting name and url : ", e)
                pass


def get_profile_details(page_source, campaign_id):
    """Function to scrape profile info """
    soup = BeautifulSoup(page_source, "html.parser")
    # print(soup.prettify())
    out = {}
    out["campaign_id"] = campaign_id
    out["name"] = ""
    out["title"] = ""
    out["company"] = ""
    out["company_url"] = ""
    out["location"] = ""
    out["email"] = ""
    out["website"] = ""
    out["phone"] = ""
    out["skype"] = ""
    out["twitter"] = ""

    #Find Profile Name
    try:
        name = soup.find("span", {"class": "profile-topcard-person-entity__name Sans-24px-black-90%-bold"}).text.strip()
        # print("Name : ",name)
        out["name"] = name
    except Exception as e:
        print("Exception in Finding Name : ", e)
        pass

    #Find Job Title
    try:
        title = soup.find("span", {"class": "profile-topcard__summary-position-title"}).text.strip()
        # print("Title : ",title)
        out["title"] = title
    except Exception as e:
        print("Exception in Finding Title : ", e)
        pass

    #  Find Current Company
    try:
        time.sleep(3)
        company = soup.find("a", {"class": "li-i18n-linkto inverse-link-on-a-light-background Sans-14px-black-75%-bold"})
        # company_url = soup.find("a",{"class":"li-i18n-linkto inverse-link-on-a-light-background Sans-14px-black-75%-bold"}).get(["href"])
        print("New :", company.text)
        print("URL :", company.get('href'))
        out["company"] = company.text
        out["company_url"] = company.get('href')
    except Exception as e:
        try:
            company = soup.find("span", {"class": "Sans-14px-black-75%-bold"})
            # print("Except : New :", company.text)
            out["company"] = company.text
        except:
            print("Exception in finding current company : ", e)
            pass

    # Finding Location
    try:
        location = soup.find("div", {"class": "profile-topcard__location-data inline Sans-14px-black-60% mr5"}).text.strip()
        # print("Location : ",location)
        out["location"]=location
    except Exception as e:
        print("Exception in finding location : ", e)
        pass

    # Finding Contact Details
    try:
        for dd in soup.find_all("dd", {"class", "profile-topcard__contact-info-item mv2 Sans-14px-black-60%"}):
            dd1 = dd.find("li-icon")
            if dd1["type"] == "envelope-icon":
                out["email"] = dd.text.strip()
            elif dd1["type"] == "link-icon":
                out["website"] = dd.text.strip()
            elif dd1["type"] == "skype-icon":
                out["skype"] = dd.text.strip()
            elif dd1["type"] == "twitter-icon":
                out["twitter"] = dd.text.strip()
            elif dd1["type"] == "mobile-icon":
                out["phone"] = dd.text.strip()
            else:
                out[dd1["type"].split("-")[0]]=dd.text.strip()
    except Exception as e:
        print("Exception in finding email : ", e)
        pass
    return out


def scrap_single_profile(url, driver, campaign_id):
    "Function to Scrap Single Profile from Linkedin"
    try:
        print("getting data for this url : ", url)
        time.sleep(random.randint(0, 10))
        driver.get(url)
        # Finding Profile Details
        time.sleep(5)
        contact_details = get_profile_details(driver.page_source, campaign_id)
        return contact_details
    except Exception as e:
        print("Exception in getting single profile details : ", e)
        pass


if __name__ == "__main__":
# def main(driver, campaign_id, username, password, keyword, geography, company_name):
    with open('main.log', 'w'):
        pass
    print("Starting Program at ", datetime.datetime.now())
    driver = webdriver.Chrome(executable_path="D:\\Python Scripts\\LeadStack.io\\assets\\chromedriver.exe")
    driver.maximize_window()

    username = "abhishek.patra@techrefic.com"
    # username = "officialrohit.03@gmail.com"
    password = "abhishek.patra"
    # password = "1sainath"
    # con_user = "anurag786rajawat@gmail.com"
    # con_pass = "Tech@refic18"
    geography = input("Enter geography - ")
    industry = input("Enter industry - ")
    seneority = input("Enter seneority - ")
    company_type = input("Enter company_type - ")
    head_count = input("Enter head_count - ")
    # no_of_search = 0
    # company_name = "PwC"
    campaign_id = 22

    driver = linkedin_login(driver=driver, username=username, password=password)

    driver = search_sn(driver=driver, geography=geography, industry=industry, seneiority=seneority, company_type=company_type, head_count=head_count)

    time.sleep(random.randint(4, 10))
    print("Finding Page Count for total results")

    extract_profiles(driver=driver, campaign_id=campaign_id)

    driver.close()